using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;
using UnityEngine.UI;
public class CreateAndJoinRoom : MonoBehaviourPunCallbacks
{
    public InputField inputCreateRoom;
    public InputField inputJoinRoom;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void CreateRoom()
    {
        Debug.Log("Create Room : " + inputCreateRoom.text);
        PhotonNetwork.CreateRoom(inputCreateRoom.text);
    }
    public void JoinRoom()
    {
        Debug.Log("Join Room: " + inputJoinRoom.text);
        PhotonNetwork.JoinRoom(inputJoinRoom.text);
    }
    public override void OnJoinedRoom()
    {
        PhotonNetwork.LoadLevel("Game");
    }

}
