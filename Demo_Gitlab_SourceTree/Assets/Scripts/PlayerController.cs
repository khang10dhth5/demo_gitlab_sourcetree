using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
public class PlayerController : MonoBehaviour
{
    public float speed;
    private float point=0;
    public Text txtPoint;
    private PhotonView photonView;
    // Start is called before the first frame update
    void Start()
    {
        photonView = gameObject.GetComponent<PhotonView>();
        if(txtPoint==null)
        {
            txtPoint = GameObject.Find("PointText").GetComponent<Text>();
        }
    }

    // Update is called once per frame
    void Update()
    {
       
        float horizontal = Input.GetAxisRaw("Horizontal");
        if(photonView.IsMine)
        {
            transform.position += new Vector3(horizontal * Time.deltaTime * speed, 0, 0);
            if (transform.position.x < -8.3)
            {
                transform.position = new Vector3(-8.3f, transform.position.y, 0);
            }
            if (transform.position.x > 8.3)
            {
                transform.position = new Vector3(8.3f, transform.position.y, 0);
            }
        }
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "point")
        {
            if (photonView.IsMine)
            {
                point++;
                
                txtPoint.text = "Point: " + point;
                
            }
            Destroy(collision.gameObject);

        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
       
    }
}
