using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class SpawnPlayer : MonoBehaviour
{
    public GameObject Player;
    // Start is called before the first frame update
    void Start()
    {
        PhotonNetwork.Instantiate(Player.name, new Vector3(Random.Range(-6, 6), transform.position.y, 0),Quaternion.identity,0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
