using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class SpawnPointController : MonoBehaviour
{
    public GameObject Point;
    // Start is called before the first frame update
    void Start()
    {
        if(PhotonNetwork.IsMasterClient)
            StartCoroutine("SpawnPoint");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator SpawnPoint()
    {
        yield return new WaitForSeconds(1);
        PhotonNetwork.Instantiate(Point.name, new Vector3(Random.Range(-8, 8), transform.position.y, 0),Quaternion.identity,0);
        StartCoroutine("SpawnPoint");
    }
}
